@echo off

if "%1%" == ""           goto help
if "%1%" == "coverage"   goto coverage
if "%1%" == "install"    goto install
if "%1%" == "migrations" goto migrations
if "%1%" == "run"        goto run
if "%1%" == "superuser"  goto superuser
if "%1%" == "test"       goto test

:help
@echo coverage   : Run code coverage, export coverage result as html
@echo install    : Install requirements
@echo migrations : Make django migrations
@echo run        : Run the project
@echo superuser  : Create a super user account on project
@echo test       : Run project tests
goto exit

:coverage
@coverage run --source='.' manage.py test
@coverage report -m
@coverage html
goto exit

:install
@pip install -r requirements.txt
goto exit

:migrations
@python manage.py makemigrations
@python manage.py migrate
goto exit

:run
@python manage.py runserver 127.0.0.1:8000
goto exit

:superuser
@python manage.py createsuperuser
goto exit

:test
@python manage.py test
goto exit

:exit
exit /b 0
