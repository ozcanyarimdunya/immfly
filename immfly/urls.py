from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from immfly.views import ChannelView, ContentDetail, ContentView

admin.site.site_header = settings.SITE_HEADER
admin.site.site_title = settings.SITE_HEADER

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("api.urls")),
    path("", ChannelView.as_view(), name="channel"),
    path("content/", ContentView.as_view(), name="content"),
    path("content-detail/", ContentDetail.as_view(), name="content-detail"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
