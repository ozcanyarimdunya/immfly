import os

from .base import *  # noqa

mode = os.getenv("MODE", "DEV")

if mode == "PROD":
    from .prod import *  # noqa
elif mode == "TEST":
    from .test import *  # noqa
else:
    from .dev import *  # noqa
