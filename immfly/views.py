from django.views.generic import TemplateView


class ChannelView(TemplateView):
    template_name = "channel.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        parent = self.request.GET.get("parent") or ""
        context.update(parent=parent)
        return context


class ContentView(TemplateView):
    template_name = "content.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        channel = self.request.GET.get("channel") or ""
        context.update(channel=channel)
        return context


class ContentDetail(TemplateView):
    template_name = "content-detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uuid = self.request.GET.get("uuid")
        context.update(uuid=uuid)
        return context
