#!/usr/bin/env bash

echo "... Applying Django migrations"
python manage.py makemigrations
python manage.py migrate

echo "... Loading initial data"
python manage.py loaddata initials

echo "... Applying Django collect static"
python manage.py collectstatic --noinput --clear

echo "... Starting Django"
gunicorn immfly.wsgi -w 4 -b :8000 --reload
