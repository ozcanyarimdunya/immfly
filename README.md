# Immfly Task

Immfly task

[![Coverage Status](https://coveralls.io/repos/gitlab/ozcanyarimdunya/immfly/badge.svg?branch=HEAD)](https://coveralls.io/gitlab/ozcanyarimdunya/immfly?branch=HEAD)

## Installation

Clone the project

```shell
git clone https://gitlab.com/ozcanyarimdunya/immfly.git
```

**[Local]**

Create a virtualenv and install the requirements

```shell
make install
```

Make the migrations and load initial data

```shell
make migrations
python manage.py loaddata initials
```

> Admin panel username: "admin", password: "123"

Now, tests!

```shell
make test # or make coverage
```

**[Docker]**

Make sure you have installed docker and docker-compose.

```shell
docker-compose up -d --build
```

Checkout [website](http://localhost:8000/)

## Usage

To export ratings

```shell
python manage.py rating -f filename.csv
```
