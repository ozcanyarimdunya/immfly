from django.test import TestCase

from api.models import Channel, Content, Group
from api.tests.utils import dummy_image


class TestModels(TestCase):
    def tearDown(self) -> None:
        Group.objects.all().delete()
        Channel.objects.all().delete()
        Content.objects.all().delete()

    def test_str(self):
        grp = Group.objects.create(name="Test group")
        self.assertEqual(str(grp), "Test group")

        channel = Channel.objects.create(title="Test channel", picture=dummy_image())
        self.assertEqual(str(channel), "Test channel")

        content = Content.objects.create(
            name="Test content",
            rating=10 / 2,
            channel=channel,
            file=dummy_image(),
        )
        self.assertEqual(str(content), "Test content")

    def test_parent_channel_with_content(self):
        parent_channel = Channel.objects.create(
            title="Parent channel", picture=dummy_image()
        )
        Channel.objects.create(
            title="Child channel 1", picture=dummy_image(), parent=parent_channel
        )
        Channel.objects.create(
            title="Child channel 2", picture=dummy_image(), parent=parent_channel
        )

        with self.assertRaises(Exception):
            Content.objects.create(
                name="Test content",
                rating=10 / 2,
                channel=parent_channel,
                file=dummy_image(),
            )

    def test_rating(self):
        channel = Channel.objects.create(title="Parent channel", picture=dummy_image())
        content = Content.objects.create(
            name="Test content",
            rating=10 / 2,
            channel=channel,
            file=dummy_image(),
        )
        self.assertEqual(content.rating, 5)
