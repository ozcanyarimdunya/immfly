from rest_framework.test import APITestCase

from api.models import Channel, Content, Group
from api.tests.utils import dummy_image


class TestChannelViewSet(APITestCase):
    def tearDown(self) -> None:
        Group.objects.all().delete()
        Channel.objects.all().delete()
        Content.objects.all().delete()

    def test_get(self):
        for i in range(10):
            Channel.objects.create(title=f"Channel {i}", picture=dummy_image())
        response = self.client.get("/api/channel/")
        self.assertEqual(len(response.data), 10)

    def test_get_by_parent(self):
        Channel.objects.create(title="Channel -", picture=dummy_image())
        Channel.objects.create(title="Channel -", picture=dummy_image())
        Channel.objects.create(title="Channel -", picture=dummy_image())
        ch1 = Channel.objects.create(title="Channel parent", picture=dummy_image())
        Channel.objects.create(title="Channel 2", picture=dummy_image(), parent=ch1)
        response = self.client.get(f"/api/channel/?parent={ch1.uuid}")
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["parent"], ch1.pk)

    def test_get_by_group(self):
        ch1 = Channel.objects.create(title="Channel 1", picture=dummy_image())
        ch2 = Channel.objects.create(title="Channel 2", picture=dummy_image())
        ch3 = Channel.objects.create(title="Channel 3", picture=dummy_image())
        ch4 = Channel.objects.create(title="Channel 4", picture=dummy_image())
        ch5 = Channel.objects.create(title="Channel 5", picture=dummy_image())

        grp1 = Group.objects.create(name="Group 1")
        grp2 = Group.objects.create(name="Group 2")

        ch1.groups.add(grp1)
        ch2.groups.add(grp1)
        ch3.groups.add(grp1)

        ch4.groups.add(grp2)
        ch5.groups.add(grp2)

        response = self.client.get("/api/channel/?group=Group 1")
        self.assertEqual(len(response.data), 3)

        response = self.client.get("/api/channel/?group=up 1")
        self.assertEqual(len(response.data), 3)

        response = self.client.get("/api/channel/?group=oup 2")
        self.assertEqual(len(response.data), 2)

    def test_retrieve(self):
        response = self.client.get("/api/channel/not-exists-uuid/")
        self.assertEqual(response.status_code, 404)
        grp1 = Group.objects.create(name="Test-1")
        parent = Channel.objects.create(
            title="test-parent-1",
            picture=dummy_image(),
        )
        ch = Channel.objects.create(
            title="test-1", picture=dummy_image(), parent=parent
        )
        ch.groups.add(grp1)

        response = self.client.get(f"/api/channel/{parent.uuid}/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["groups"]), 1)


class TestContentViewSet(APITestCase):
    def tearDown(self) -> None:
        Group.objects.all().delete()
        Channel.objects.all().delete()
        Content.objects.all().delete()

    def test_get(self):
        ch1 = Channel.objects.create(title="Channel 1", picture=dummy_image())
        ch2 = Channel.objects.create(
            title="Channel 2", picture=dummy_image(), parent=ch1
        )
        ch3 = Channel.objects.create(
            title="Channel 3", picture=dummy_image(), parent=ch1
        )
        ch4 = Channel.objects.create(
            title="Channel 4", picture=dummy_image(), parent=ch3
        )
        ch5 = Channel.objects.create(
            title="Channel 5", picture=dummy_image(), parent=ch3
        )
        ch6 = Channel.objects.create(
            title="Channel 5", picture=dummy_image(), parent=ch5
        )

        Content.objects.create(name="Content 1", channel=ch2)
        Content.objects.create(name="Content 2", channel=ch2)
        Content.objects.create(name="Content 3", channel=ch4)
        Content.objects.create(name="Content 4", channel=ch4)
        Content.objects.create(name="Content 5", channel=ch4)
        Content.objects.create(name="Content 6", channel=ch4)
        Content.objects.create(name="Content 7", channel=ch6)

        response = self.client.get(f"/api/content/?channel={ch4.uuid}")
        self.assertEqual(len(response.data), 4)
