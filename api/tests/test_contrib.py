from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.test import TestCase

from api.contrib.handlers import api_exception_handler
from api.contrib.rating import Rating
from api.contrib.utils import safe_division
from api.models import Channel, Content, Group
from api.tests.utils import dummy_image


class TestContrib(TestCase):
    def tearDown(self) -> None:
        Group.objects.all().delete()
        Channel.objects.all().delete()
        Content.objects.all().delete()

    def test_exception_handler(self):
        response = api_exception_handler(Exception("Error"), context=None)
        self.assertEqual(
            response.data, {"error": True, "message": "Error", "status_code": 500}
        )

        response = api_exception_handler(Http404("Error"), context=None)
        self.assertEqual(response.status_code, 404)

        response = api_exception_handler(PermissionDenied("Error"), context=None)
        self.assertEqual(response.status_code, 403)

    def test_safe_division(self):
        self.assertEqual(safe_division(10, 5), 2)
        self.assertEqual(safe_division(10, 0), 0)
        self.assertEqual(safe_division(10, 0, 1), 1)

    def test_rating(self):
        ch1 = Channel.objects.create(
            title="Channel 1", picture=dummy_image(), parent=None
        )
        ch2 = Channel.objects.create(
            title="Channel 2", picture=dummy_image(), parent=ch1
        )
        ch3 = Channel.objects.create(
            title="Channel 3", picture=dummy_image(), parent=ch1
        )
        ch4 = Channel.objects.create(
            title="Channel 4", picture=dummy_image(), parent=ch3
        )
        ch5 = Channel.objects.create(
            title="Channel 5", picture=dummy_image(), parent=ch3
        )
        ch6 = Channel.objects.create(
            title="Channel 6", picture=dummy_image(), parent=None
        )
        ch7 = Channel.objects.create(
            title="Channel 7", picture=dummy_image(), parent=ch6
        )

        Content.objects.create(name="Content 1", channel=ch2, rating=10 / 5)
        Content.objects.create(name="Content 2", channel=ch2, rating=10 / 8)

        Content.objects.create(name="Content 3", channel=ch4, rating=10 / 10)
        Content.objects.create(name="Content 4", channel=ch4, rating=10 / 5)
        Content.objects.create(name="Content 5", channel=ch4, rating=10 / 6)
        Content.objects.create(name="Content 6", channel=ch4, rating=10 / 7)

        Content.objects.create(name="Content 7", channel=ch5, rating=10 / 5)

        Content.objects.create(name="Content 8", channel=ch7, rating=10 / 5)
        Content.objects.create(name="Content 9", channel=ch7, rating=10 / 4)
        Content.objects.create(name="Content 10", channel=ch7, rating=10 / 3)
        Content.objects.create(name="Content 11", channel=ch7, rating=10 / 2)
        Content.objects.create(name="Content 12", channel=ch7, rating=10 / 1)

        """
        Ratings:

        ch7: No subchannels, content 8,9,10,11,12
            ((10/5) + (10/4) + (10/3) + (10/2) + (10/1)) / 5 = 4.566666666666667

        ch6: subchannels 7
            4.566666666666667 / 1 = 4.566666666666667

        ch5: No subchannels, content 7
            (10/5) / 1 = 2.0

        ch4: No subchannels, content 3,4,5,6
            ((10/10) + (10/5) + (10/6) + (10/7)) / 4 = 1.523809523809524

        ch3: subchannels 4,5
            (1.523809523809524 + 2.0) / 2 = 1.7619047619047619

        ch2: No subchannels, content 1,2
            ((10/5) + (10/8)) / 2 = 1.625

        ch1: subchannels 2,3
            (1.625 + 1.7619047619047619) / 2 = 1.693452380952381

        """
        rating = Rating().get()
        simple = {k: round(v, 2) for k, v in rating}

        self.assertEqual(simple[ch1], 1.69)
        self.assertEqual(simple[ch2], 1.62)
        self.assertEqual(simple[ch3], 1.76)
        self.assertEqual(simple[ch4], 1.52)
        self.assertEqual(simple[ch5], 2.00)
        self.assertEqual(simple[ch6], 4.57)
        self.assertEqual(simple[ch7], 4.57)
