from django.db import models


class ChannelManager(models.Manager):
    def by(self, group, parent):
        query = self
        if group:
            query = query.filter(groups__name__icontains=group)

        if parent:
            query = query.filter(parent__uuid=parent)
        else:
            query = query.filter(parent=None)
        return query


class ContentManager(models.Manager):
    def by(self, channel):
        query = self
        if channel:
            query = query.filter(channel__uuid=channel)
        return query
