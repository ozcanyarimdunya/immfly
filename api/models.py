import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.managers import ChannelManager, ContentManager


class TimestampedModel(models.Model):
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        unique=True,
        verbose_name=_("Uuid"),
    )
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("Created at"),
    )
    updated = models.DateTimeField(
        auto_now=True,
        verbose_name=_("Updated at"),
    )

    class Meta:
        abstract = True


class Group(TimestampedModel):
    class Meta:
        ordering = ["-created"]
        verbose_name = _("Group")
        verbose_name_plural = _("Groups")

    name = models.CharField(
        max_length=120,
        verbose_name=_("Group name"),
    )

    def __str__(self):
        return self.name


class Channel(TimestampedModel):
    class Meta:
        ordering = ["-created"]
        verbose_name = _("Channel")
        verbose_name_plural = _("Channels")

    title = models.CharField(
        max_length=120,
        verbose_name=_("Title"),
    )
    language = models.CharField(
        max_length=120,
        default="en",
        verbose_name=_("Language"),
    )
    picture = models.ImageField(
        upload_to="channel/",
        verbose_name=_("Picture"),
    )
    parent = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        related_name="subchannels",
        on_delete=models.CASCADE,
        verbose_name=_("Parent"),
    )
    groups = models.ManyToManyField(
        Group,
        verbose_name=_("Groups"),
        blank=True,
    )

    objects = ChannelManager()

    def __str__(self):
        return self.title


class Content(TimestampedModel):
    class Meta:
        ordering = ["-created"]
        verbose_name = _("Content")
        verbose_name_plural = _("Contents")

    name = models.CharField(
        max_length=240,
        verbose_name=_("Name"),
    )
    metadata = models.JSONField(
        default=dict,
        blank=True,
        verbose_name=_("Metadata"),
    )
    rating = models.FloatField(
        default=0.0,
        verbose_name=_("Rating"),
    )
    channel = models.ForeignKey(
        Channel,
        related_name="contents",
        on_delete=models.CASCADE,
        verbose_name=_("Channel"),
    )
    file = models.FileField(
        upload_to="content/",
        verbose_name=_("File"),
    )

    objects = ContentManager()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.channel.subchannels.count():
            raise Exception("Channel is a parent channel, cannot add content!")
        super().save(*args, **kwargs)
