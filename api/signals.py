from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from api.models import Channel


@receiver(m2m_changed, sender=Channel.groups.through)
def on_groups_changed(sender, instance: Channel, action, **kwargs):
    # fixme: check parents children
    if action not in ("post_add", "post_remove", "post_clear"):
        return
    if not instance.parent:
        return
    instance.parent.groups.set(instance.groups.all())
