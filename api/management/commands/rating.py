import csv
import os
import warnings

from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import ugettext_lazy as _

from api.contrib.rating import Rating


class Command(BaseCommand):
    help = "Calculate Rating for Channels"

    def add_arguments(self, parser):
        parser.add_argument("--filename", "-f", help="Export file name")

    def handle(self, *args, **options):
        filename = options.pop("filename")
        if not filename:
            raise CommandError(
                _("You should specify a filename with -f/--filename <filename>.")
            )

        root, ext = os.path.splitext(filename)
        if ext != ".csv":
            warnings.warn(
                _(f"Extension for given file is {ext}, but it will save as csv."),
                RuntimeWarning,
            )

        self.stdout.write(self.style.SUCCESS("START".ljust(10, ".")))
        try:
            rating = Rating()
            result = rating.get()
        except Exception as ex:
            raise CommandError(_(f"Unable to get rating: {ex}"))

        with open(filename, mode="w", encoding="utf-8", newline="") as fp:
            writer = csv.writer(fp)
            writer.writerow(["Channel title", "Average rating"])  # noqa
            writer.writerows([(ch.title, round(rt, 2)) for ch, rt in result])

        self.stdout.write(
            self.style.SUCCESS(_(f"Result exported successfully to {filename} ."))
        )
        self.stdout.write(self.style.SUCCESS("FINISH".ljust(10, ".")))
