from django.contrib import admin

from api.models import Channel, Content, Group

admin.site.register(Group)
admin.site.register(Channel)
admin.site.register(Content)
