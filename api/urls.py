from django.urls import path

from api import views

app_name = "api"
urlpatterns = [
    path(
        "channel/",
        views.ChannelListAPIView.as_view(),
        name="channel-list",
    ),
    path(
        "channel/<uuid:uuid>/",
        views.ChannelRetrieveAPIView.as_view(),
        name="channel-detail",
    ),
    path(
        "content/",
        views.ContentListAPIView.as_view(),
        name="content-list",
    ),
    path(
        "content/<uuid:uuid>/",
        views.ContentRetrieveAPIView.as_view(),
        name="content-detail",
    ),
]
