def safe_division(num1, num2, default=0):
    try:
        return num1 / num2
    except ZeroDivisionError:
        return default
