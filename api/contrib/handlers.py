import logging

from django.core.exceptions import PermissionDenied
from django.http import Http404
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.views import set_rollback

logger = logging.getLogger(__name__)


def api_exception_handler(exc, context):  # noqa
    if isinstance(exc, Http404):
        exc = exceptions.NotFound()
    elif isinstance(exc, PermissionDenied):
        exc = exceptions.PermissionDenied()

    if not getattr(exc, "status_code", None):
        setattr(exc, "status_code", 500)

    error_data = {
        "error": True,
        "message": getattr(exc, "detail", str(exc)),
        "status_code": exc.status_code,
    }
    set_rollback()
    logger.error(error_data)
    return Response(error_data, status=exc.status_code)
