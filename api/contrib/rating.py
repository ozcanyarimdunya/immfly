from django.db.models import Avg

from api.contrib.utils import safe_division
from api.models import Channel


class Rating:
    def __init__(self):
        queryset = Channel.objects.all()
        self.store = {}
        self.data = self.calculate(queryset)

    def calculate(self, channels):
        for channel in channels:
            if channel in self.store.keys():
                return

            if channel.subchannels.count():
                yield from self.calculate(channel.subchannels.all())

            if channel.contents.count():
                aggregate = channel.contents.all().aggregate(rating=Avg("rating"))
                rating = aggregate["rating"]
            else:
                subchannels = channel.subchannels.all()
                rating = sum(
                    [value for key, value in self.store.items() if key in subchannels]
                )
                rating = safe_division(rating, len(subchannels))

            self.store[channel] = rating
            yield channel, rating

    def get(self):
        result = sorted(self.data, key=lambda it: it[1], reverse=True)
        return result
