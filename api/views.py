from rest_framework.generics import ListAPIView, RetrieveAPIView

from api.models import Channel, Content
from api.serializers import ChannelSerializer, ContentSerializer


class ChannelListAPIView(ListAPIView):
    serializer_class = ChannelSerializer
    lookup_field = "uuid"

    def get_queryset(self):
        parent = self.request.query_params.get("parent")
        group = self.request.query_params.get("group")
        return (
            Channel.objects.by(group=group, parent=parent)
            .select_related("parent")
            .prefetch_related("groups")
            .all()
        )


class ChannelRetrieveAPIView(RetrieveAPIView):
    serializer_class = ChannelSerializer
    lookup_field = "uuid"
    queryset = Channel.objects.all()


class ContentListAPIView(ListAPIView):
    serializer_class = ContentSerializer
    lookup_field = "uuid"

    def get_queryset(self):
        channel = self.request.query_params.get("channel")
        return (
            Content.objects.by(channel=channel)
            .select_related("channel")
            .prefetch_related("channel__groups")
            .all()
        )


class ContentRetrieveAPIView(RetrieveAPIView):
    serializer_class = ContentSerializer
    lookup_field = "uuid"
    queryset = Content.objects.all()
