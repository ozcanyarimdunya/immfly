from rest_framework import serializers

from api.models import Channel, Content, Group


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ("name",)


class ParentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = ("title",)


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = "__all__"

    def to_representation(self, instance: Channel):
        representation = super().to_representation(instance)
        representation.update(
            groups=GroupSerializer(instance=instance.groups.all(), many=True).data,
            type="SUB" if instance.subchannels.count() else "CNT",
        )
        return representation


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = "__all__"

    def to_representation(self, instance: Content):
        representation = super().to_representation(instance)
        representation.update(rating=instance.rating)
        return representation
